# What is Dimgi?

Dimgi is a platform that helps you get AI superpowers with standard templates. Specially conceived for companies using drones, you won't need an army of AI engineers anymore to detect objects from images.

With standardized templates and without human intervention you will be able to take surface measures from roof, count thousands of tree, manage biodiversity and much more.

## Getting Started <a href="#getting-started" id="getting-started"></a>

**Got 2 minutes?** Check out a video overview of Dimgi AI Platform:

{% embed url="https://youtu.be/aGv4TYvV_Ig" %}
Perform basic inferences using standard DL models on Dimgi AI platform.
{% endembed %}

### Guides: Jump right in <a href="#guides-jump-right-in" id="guides-jump-right-in"></a>

Follow our handy guides to get started on the basics as quickly as possible:

{% content-ref url="guides/creating-your-first-project.md" %}
[creating-your-first-project.md](guides/creating-your-first-project.md)
{% endcontent-ref %}

{% content-ref url="guides/uploading-your-data.md" %}
[uploading-your-data.md](guides/uploading-your-data.md)
{% endcontent-ref %}

{% content-ref url="guides/launching-your-first-template.md" %}
[launching-your-first-template.md](guides/launching-your-first-template.md)
{% endcontent-ref %}

{% content-ref url="guides/collaborating.md" %}
[collaborating.md](guides/collaborating.md)
{% endcontent-ref %}
