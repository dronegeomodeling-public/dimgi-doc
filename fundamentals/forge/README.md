# Forge

## Available Templates <a href="#available-templates" id="available-templates"></a>

In the Forge section you'll find all available templates. Templates work as tools you can use to tackle challenges related to business processes and getting insights for decision-making.

This is the list of available templates at the time:

{% content-ref url="object-detection-on-images.md" %}
[object-detection-on-images.md](object-detection-on-images.md)
{% endcontent-ref %}
