# Table of contents

* [What is Dimgi?](README.md)

## Guides

* [Creating your first project](guides/creating-your-first-project.md)
* [Uploading your data](guides/uploading-your-data.md)
* [Launching your first template](guides/launching-your-first-template.md)
* [Collaborating](guides/collaborating.md)

## Fundamentals

* [Projects](fundamentals/projects.md)
* [Storage](fundamentals/storage.md)
* [Forge](fundamentals/forge/README.md)
  * [Object Detection on Images](fundamentals/forge/object-detection-on-images.md)
  * [Object Detection on Videos](fundamentals/forge/object-detection-on-videos.md)
  * [Measure Roof Dimensions](fundamentals/forge/measure-roof-dimensions.md)
  * [Detect and Count Trees](fundamentals/forge/detect-and-count-trees.md)
  * [Custom Templates](fundamentals/forge/custom-templates.md)
* [Results](fundamentals/results.md)

## Use Cases

* [For Small Teams](use-cases/for-small-teams.md)
* [For Engineers](use-cases/for-engineers.md)
