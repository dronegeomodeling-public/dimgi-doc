# Launching your first template

## Set a Dimgi template

### Select a template <a href="#select-a-template" id="select-a-template"></a>

At your left you should be able to identify the current selected project. The template will run only for data stored into this space.

You can either select a last used template located in the quick access on top of the page or select other template in the template section. A green button must be visible to select the template. If the button is gray, that means you do not subscribe for this template.

There are two free templates in order to have fun detecting objects on images and videos.

### Select data <a href="#select-data" id="select-data"></a>

Depending on the template you choose you must select at least one image or one video for your stored data. Remember this data is relied to the project you selected before.

### Set parameters <a href="#set-parameters" id="set-parameters"></a>

Parameters depends on the template you selected, the main inputs you can modify are model to use, thearshold, among others.

### Check inputs <a href="#check-inputs" id="check-inputs"></a>

After setting the parameters you can check if all entered inputs are correct and after checking those inputs, you can back to previous page clicking on Back Button and correct any of the inputs.

