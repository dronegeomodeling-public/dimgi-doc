# Uploading your data

## Upload data into Dimgi

After clicking in the Storage Section at your left, you can have a look at all the files into your project space (To understand how it works take a look at [`Fundamentals -> Projects`](../fundamentals/projects.md)``

To Upload a file just click in the send button and once you are in the upload space click the green "cloud-with-arrow-up" :smile: button at your right. That it's, you can select files you want to upload and take a coffe while transmission goes on.
