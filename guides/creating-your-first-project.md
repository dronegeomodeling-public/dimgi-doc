# Creating your first project

## Create a project with Dimgi

### The basics <a href="#the-basics" id="the-basics"></a>

Dimig is a **data-centric** application. Projects are containers for your data. Think of them as a place to store everything your team need to get done and complete a project.

Moreover, data resulted from running templates are also stored into your project. In one workspace you get access  to input and result data.&#x20;

### Creating a project <a href="#creating-a-project" id="creating-a-project"></a>

Hit the big green '+' button in your sidebar and select 'New Project' from the menu that pops up. Give your project a name, and you're good to go!
